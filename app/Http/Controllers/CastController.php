<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function index(){
        $cast = Cast::all();
 
        return view('cast.index',compact('cast'));
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $validatedData = $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.max' => 'Nama tidak boleh lebih dari 45 karakter',
            'umur.required'  => 'Umur harus diisi',
            'umur.integer'  => 'Umur harus angka',
            'bio.required'  => 'Biodata harus diisi',
        ]
        );

        $cast = new cast;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
        $cast->save();
        
        return redirect('/cast');
    }

    public function show($id){
        $cast = Cast::find($id);
        return view('cast.show',compact('cast'));
    }

    public function edit($id){
        $cast = Cast::find($id);
        return view('cast.edit',compact('cast'));
    }

    public function update(Request $request,$id){
        $validatedData = $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'integer',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama harus diisi',
            'nama.max' => 'Nama tidak boleh lebih dari 45 karakter',
            'umur.required'  => 'Umur harus diisi',
            'umur.integer'  => 'Umur harus angka',
            'bio.required'  => 'Biodata harus diisi',
        ]
        );

        $cast = Cast::find($id);
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio=$request['bio'];
        $cast->save();

        return redirect('/cast');
    }

    public function destroy($id){
        $cast = Cast::find($id);
        //$cast->nama = $request['nama'];
        //$asking= $this->anticipate('Hapus?', ['Yes', 'No']);
        
        $cast->delete();

        return redirect('/cast');
    }
}
