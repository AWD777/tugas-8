@extends('layout.master')
@section('judul')
    <p>Edit Detail Cast</p>
@endsection
    
@section('content')
    <form action='/cast/{{$cast->id}}' method='POST'>
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="formGroupNamaInput">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Nama Lengkap">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="formGroupUmurInput2">Umur</label>
            <input type="text" class="form-control" name='umur' value="{{$cast->umur}}" placeholder="Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Biodata</label>
            <textarea class="form-control" name='bio' placeholder="Biodata "rows="3">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
    
