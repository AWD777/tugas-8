@extends('layout.master')
@section('judul')
    <p>Detail Cast</p>
@endsection
    
@section('content')
    <div class="d-flex justify-content-center"">
        <h4>Detail Data</h4>
    </div>
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th style="width: 20px">Nama</th>
                <td>{{$cast->nama}}</td>
            </tr>
            <tr>
                <th>Umur</th>
                <td>{{$cast->umur}}</td>
            </tr>
            <tr>
                <th>Biodata</th>
                <td>{{$cast->bio}}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <div class="d-flex justify-content-end"">
        <a href="/cast" class="btn btn-primary mb-sm">Kembali</a>
    </div>
@endsection