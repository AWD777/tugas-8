@extends('layout.master')
@section('judul')
    <p>Tambah Cast</p>
@endsection
    
@section('content')
    <form action='/cast' method='POST'>
        @csrf
        <div class="form-group">
            <label for="formGroupNamaInput">Nama</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="formGroupUmurInput2">Umur</label>
            <input type="text" class="form-control" name='umur' placeholder="Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Biodata</label>
            <textarea class="form-control" name='bio' placeholder="Biodata "rows="3"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endsection
    
