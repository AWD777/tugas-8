@extends('layout.master')

@section('judul')
    Data Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Biodata</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>{{$item->umur}}</td>
                    <td>{{$item->bio}}</td>
                    <td>
                        <form class="mt-2" action="/cast/{{$item->id}}" method="POST">
                            <a href="/cast/{{$item->id}}" class="btn btn-info sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning sm">Edit</a>
                            @csrf
                            @method("DELETE")
                            <button type="submit" class="btn btn-danger mb-sm">Hapus</button>
                        </form>
                    </td>
                </tr>
            @empty
                <h1>Data tidak ada</h1>
            @endforelse
        </tbody>
    </table>
@endsection


