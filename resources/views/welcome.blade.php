@extends('layout.master')

@section('judul')
    <p>Halaman Index</p>
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$firstname}} {{$lastname}}</h1>
    <h5>Terimakasih telah bergabung di Website Kami. Media Belajar kita bersama!</h5>
@endsection