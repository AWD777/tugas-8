@extends('layout.master')

@section('judul')
    <p>Halaman Form</p>
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection